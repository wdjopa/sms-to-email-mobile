# **SMS To Email**

https://chat.openai.com/share/b4003c83-999e-4347-8ab1-45a1671ce697

## **Overview**

SMS To Email is a mobile application designed to seamlessly transfer SMS messages received on your phone to a specified email address. This app is particularly useful for archiving SMS messages or for sharing important information received via SMS with your email contacts.

## **Features**

- **SMS Forwarding:** Automatically forwards incoming SMS messages to an email address.
- **Email List Management:** Manage a list of email recipients who will receive the forwarded SMS messages.
- **Customizable Settings:** Configure the email service URL and other settings to suit your preferences.

## **Installation**

### **Prerequisites**

- Ensure you have Flutter installed on your machine. To get Flutter, visit [Flutter's official site](https://flutter.dev/docs/get-started/install).
- A code editor such as VSCode or Android Studio.

### **Steps**

1. Clone the repository to your local machine:
    
    ```
    shCopy code
    git clone https://github.com/your-repository-url.git
    ```
    
2. Navigate to the project directory:
    
    ```
    shCopy code
    cd sms-to-email
    ```
    
3. Fetch all the dependencies:
    
    ```
    shCopy code
    flutter pub get
    ```
    
4. Run the application:
    
    ```
    shCopy code
    flutter run
    ```
    

## **Configuration**

Before using the app, you need to set up an email service:

1. Update the email service URL in the **`lib/constants.dart`** file.
2. Customize the usage in the **`lib/sms.service.dart`** file as per your requirements.

## **Usage**

After installation and configuration, run the app. You can add email addresses to the list via the app interface. Any SMS received will be automatically forwarded to these addresses.

## **Contributing**

Contributions to the project are welcome! If you have suggestions or improvements, feel free to fork the repository and submit a pull request.

## **Contact**

For assistance or to report issues with the app, contact me at [wdjopa@lamater.tech](mailto:wdjopa@lamater.tech).