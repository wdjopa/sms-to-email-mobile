import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:sms_redirect/sms.service.dart';
import 'package:telephony/telephony.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SMS to Email',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final Telephony telephony = Telephony.instance;
  List<Map> emails = [];

  @override
  void initState() {
    super.initState();
    listenToSmsInBackground();
    loadEmails();
  }

  void listenToSmsInBackground() async {
    await telephony.requestSmsPermissions;

    telephony.listenIncomingSms(
      onNewMessage: (SmsMessage message) {
        if (kDebugMode) {
          print("New message received : ${message.body}");
        }
        sendEmail(message.body);
      },
      onBackgroundMessage: backgroundMessageHandler,
      listenInBackground: true,
    );
  }

  void loadEmails() async {
    final prefs = await SharedPreferences.getInstance();
    final String? emailsJson = prefs.getString('emails');
    if (emailsJson != null) {
      setState(() {
        emails = List.from(json.decode(emailsJson));
      });
    }
  }

  void saveEmails() async {
    final prefs = await SharedPreferences.getInstance();
    final String emailsJson = json.encode(emails);
    prefs.setString('emails', emailsJson);
  }

  void addEmail(String email, String name) {
    setState(() {
      emails.add({'email': email, 'name': name});
      saveEmails();
    });
  }

  void deleteEmail(int index) {
    setState(() {
      emails.removeAt(index);
      saveEmails();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('SMS To Email'),
      ),
      body: emails.isEmpty
          ? const Center(
              child:
                  Text('Nothing to show here. Click on + to add a new email'))
          : ListView.builder(
              itemCount: emails.length,
              itemBuilder: (context, index) {
                final item = emails[index];
                return ListTile(
                  title: Text(item['email']!),
                  subtitle: Text(item['name']!),
                  trailing: IconButton(
                    icon: const Icon(Icons.delete),
                    onPressed: () => deleteEmail(index),
                  ),
                );
              },
            ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _showAddEmailDialog(),
        child: const Icon(Icons.add),
      ),
    );
  }

  void _showAddEmailDialog() {
    showDialog(
      context: context,
      builder: (context) {
        String email = '';
        String name = '';
        return AlertDialog(
          title: const Text('Add an Email'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              TextField(
                onChanged: (value) => email = value,
                decoration: const InputDecoration(labelText: 'Email'),
              ),
              TextField(
                onChanged: (value) => name = value,
                decoration: const InputDecoration(labelText: 'Name'),
              ),
            ],
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Cancel'),
              onPressed: () => Navigator.of(context).pop(),
            ),
            TextButton(
              child: const Text('Addd'),
              onPressed: () {
                addEmail(email, name);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
