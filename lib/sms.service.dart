import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:sms_redirect/constants.dart';
import 'package:telephony/telephony.dart';
import 'package:http/http.dart' as http;

void backgroundMessageHandler(SmsMessage message) {
  print("Message reçu en arrière-plan : ${message.body}");
  sendEmail(message.body);
}

void sendEmail(String? messageBody) async {
  var url = Uri.parse(EMAIL_SERVICE_URL);
  final prefs = await SharedPreferences.getInstance();
  final String? emailsJson = prefs.getString('emails');
  try {
    if (emailsJson != null && emailsJson.isNotEmpty) {
      for (Map emailJson in List.from(json.decode(emailsJson))) {
        http.Response response = await http.post(url,
            body: jsonEncode({
              'toEmail': emailJson["email"].toString(),
              'toName': emailJson["name"].toString(),
              'message': messageBody,
              'subject': 'Un nouveau message est arrivé'
            }),
            headers: {'Content-Type': 'application/json'});
        print(response.body);
      }
    }
  } catch (e) {
    print("Erreur lors de l'envoi de l'email: $e");
  }
}
